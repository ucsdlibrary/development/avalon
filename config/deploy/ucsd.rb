# Example:
# DB=postgres REPO=git@github.com:user/avalon.git BRANCH=deploy/prod USER=deploy APP_HOST=avalon.example.edu cap dynamic deploy

set :rails_env, ENV['RAILS_ENV'] || 'production'
# Ensure we install optional gems for database and z39.50
set :bundle_flags,  "--with mysql zoom"
set :bundle_without, ENV['RAILS_ENV'] == "development" ? "production" : 'development test debug'
set :repo_url, ENV['REPO']
set :branch, ENV['BRANCH']
set :deploy_to, ENV['DEPLOY_TO']
set :hls_dir, ENV['HLS_DIR']
set :user, ENV['USER']
set :yarn_flags, "--#{ENV['RAILS_ENV']}"
server ENV['APP_HOST'], roles: %w{web app db}, user: ENV['USER'] || 'avalon'

# Unset these files from config/deploy.rb as we don't use them
remove :linked_files, "Gemfile.local", "config/*.yml", "config/*/*.yml", "config/initializers/*.rb", "public/robots.txt"
remove :linked_dirs, 'log', 'tmp'

# Set a custom tmp folder for capistrano
# see: https://github.com/capistrano/capistrano/issues/687#issuecomment-195357591
set :tmp_dir, "/home/avalon/tmp"

set :workers, { "*" => 2 }
