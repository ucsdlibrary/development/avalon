# SETTINGS__AUTH__CONFIGURATION__UCSD__NAME=UC San Diego
# SETTINGS__AUTH__CONFIGURATION__UCSD__PROVIDER=google_oauth2
# SETTINGS__AUTH__CONFIGURATION__UCSD__PARAMS__CLIENT_ID=client_id_value
# SETTINGS__AUTH__CONFIGURATION__UCSD__PARAMS__CLIENT_SECRET=client_secret_value
#
User.instance_eval do
  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    logger.debug "#{access_token.inspect}"

    email = access_token.info.email
    # Ares DB needs to query using user ID.
    # TODO: confirm that downcase'd usernames match in Ares
    username = email.split('@').first.downcase


    user = User.find_or_create_by_username_or_email(username, email)
    raise "Finding user (#{ user }) failed: #{ user.errors.full_messages }" unless user.persisted?

    user
  end
end
